# CSS

## Spécificité

Classes locales > id > class > éléments

## Sélecteurs : combinateurs

_Pseudoclasses_ : `:hover`
_Combinateur descendant_ : `div span` pour n'importe quel span dans un élément div
_Combinateur enfant_ : `ul.list > li` pour les li dans une ul de classe "list"

[Sur MDN](https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Selectors#Combinators)

## Convention de nommage BEM : utiliser des blocs

Sélecteurs BEM toujours sous forme de classes.

### Blocs et leurs éléments

Le nom d’un élément doit indiquer deux choses :

- son bloc parent, suivi d’un double underscore (aussi appelé “dunders”) ;
- la fonction de l’élément.

> Exemples : `.proj-prev__heading` ou `form__label`

### Modificateurs

Nom du bloc parent + `--` + style graphique du modificateur.

> Exemple : `.proj-rev--mint` pour une version couleur vert menthe

# SASS

Variables identifiées par le préfixe dollar ($) et les fonctions identifiées avec l’arobase (@)

### Variables

`$mint: #15DEA5;` pour stocker la couleur mint, utilisable à la place des hex codes après

### Boucles

`$colours: (mint: #15DEA5, navy: #001534 ...);`
`@each $colour, $hex in $colours {`
` .btn--#{$colour} {`
` background-color: $hex;`
` }`
`}`

### Structures conditionnelles

`@if (lightness(#15DEA5) > 25%) { ... }@else{ ... }`
